package com.example.ali.tasktimer;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddEditActivityFragment extends Fragment {
    private static final String TAG = "AddEditActivityFragment";

    public enum FragmentEditMode {EDIT , ADD}
    private FragmentEditMode mMode;

    private EditText mNameTextView;
    private EditText mDescriptionTextView;
    private EditText mSortOrderTextView;
    private Button mSaveButton;

    private OnSaveClicked mSaveListener = null;//nemidoonam vase chi gozashte filme 235 va 236
    //chon baz mikhast az callback estefade kone interface tarif kard va chon callback az in class ast dar inja tarifesh kard va hamoon tor ke ghablan didim bayad ye field besazim ke yek instance az
    //interface bashe ke bala kardim va bedimesh oon ro be constructor vali chon dar fragment nemishe be constructor chizi dad oon ro dar onAttach kardim

    /**
     * alan ma in onSaveClicked ro ham too mainActivity darim ham too AddEditAcivity vali az koja mifahme ke vaghti roo save zadd bayad kodoom ro run kone?
     * zamani ke too landscape hastim az main activity mostaghim miyad va inja ro baz mikone va vaghti inja baz shod in fragment michasbe be main activiy va tabe onAttach run mishe ke too oonja besh migim
     * ke mSaveListener = (OnSaveClicked) getActivity; yani in instance az interface oon activity ke sedash zade ro migire vase hamin ma too main behesh redidegi kardim
     * amma vaghti too portrait has miyad avval addedit activity ro run mikone bad ooon miyad va inja ro run mikone pas getActivity mishe addedit activity va mifahme ke oonja ro bayad run kone
     * ma dar main activity fragment ro remove kadim vail dar protrait lazem nis vaghat mibandimesh ba finish();
     */
    interface OnSaveClicked {
        void onSaveClicked();
    }


    public AddEditActivityFragment() {
        Log.d(TAG, "AddEditActivityFragment: constructor called");
    }

    public boolean canClose(){//in tabe dar main activity vase onBackPressed estefade shode
        return false;
    }

    @Override
    //in class vase zamani ke fragment michasbe be activity
    public void onAttach(Context context) {//nemidoonam vase chi gozashte filme 235 va 236
        Log.d(TAG, "onAttach: starts");
        super.onAttach(context);

        //activitis contaning this fragment must implement it's callback
        Activity activity = getActivity();//Return the Activity this fragment is currently associated with. : yani miyad oon activity ke baes shode in fragment ro seda bezane migire
        Log.d(TAG, "onAttach: tttttttttttttttttttttt" + activity);//in ro bebin ta getActiviy ro befahmi
        Log.d(TAG, "onAttach: tttttttttttttttttttttteeeeeeeeeeeeee" + activity.toString());
        if (!(activity instanceof OnSaveClicked)){//inja barresi mikone ke aya oon class implement karde ya na
            throw new ClassCastException(activity.getClass().getSimpleName() + " must implemented AddEditActiviyFragment.OnsaveClicked inteface");
        }
        mSaveListener = (OnSaveClicked) activity;//khob mese ghabl amal kardim inja hamoon callback ghabl ke cast karde boodim inja migim vaghti ke fragment chasbid be activity in karo bokon
        //in yani callback alan mSaveListener ma oon activity ro dare ke inro seda zade
    }

    @Override
    //vase zamani ke fragment az activity joda mishe
    public void onDetach() {//nemidoonam vase chi gozashte filme 235 va 236
        Log.d(TAG, "onDetach: starts");
        super.onDetach();
        mSaveListener = null;//migim vaghti ke fragment joda shod in karo bokon
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: startas");
        View view = inflater.inflate(R.layout.fragment_add_edit, container, false);

        mNameTextView = (EditText) view.findViewById(R.id.addedit_name);//deghat dashte bash ke goftim view.find
        mDescriptionTextView = (EditText) view.findViewById(R.id.addedit_description);
        mSortOrderTextView = (EditText) view.findViewById(R.id.addedit_sortorder);
        mSaveButton = (Button) view.findViewById(R.id.addedit_save);

        //Bundle arguments = getActivity().getIntent().getExtras();//in dare az putextra miyad ke too mainActivity sakhtim
        //inja extra ro migirim va ghablan ham gofte shode ke bundle has(Bundle getExtras ())

        Bundle arguments = getArguments();//in miyad oon bundle ke too mainActivity gozashtim ba setAtgumetn ro migire

        final Tasks tasks;//in bayad final bashe chon mikhayem azesh dar onclick estefade konim va chon yek inner class ast in bayad final bashe yade bashe in mohemme
        if (arguments != null){
            Log.d(TAG, "onCreateView: retrieving task details");
            //hatta age argument dashte bashim garanti nis ke tasks dashte bashim vase hamin task ro migirim va check mikonim ke null nabashe
            tasks = (Tasks) arguments.getSerializable(Tasks.class.getSimpleName());//inja besh key ro midim ta value ro bede
            if (tasks != null){
                Log.d(TAG, "onCreateView: Task details found editing");
                mNameTextView.setText(tasks.getName());
                mDescriptionTextView.setText(tasks.getDescription());
                mSortOrderTextView.setText(Integer.toString(tasks.getSortOrder()));//inja chon int has be string tabdilesh kardim
                mMode = FragmentEditMode.EDIT;
            } else{
                //no task so we must be adding a new task and not editing an existing one
                mMode = FragmentEditMode.ADD;
            }
        } else{
            tasks = null;//chon in dar onclick estefade mishe hatman bayad in ja ham tarif beshe va meghdar dahi she age nashe dar onClick error migire
            Log.d(TAG, "onCreateView: no arguments adding new record");
            mMode =FragmentEditMode.ADD;
        }

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //update the database if at least one field changed
                boolean add_successfull = false;//azesh too onclick estefade kardam vase nemayesh toast
                int int_sortorder;//chon dar bala ma sortOrder ro string kardim vali dar database bayad int bezarim pas inja miyaem check mikonim age has ke intesh mikonim age ham nis ke 0 besh midim
                if (mSortOrderTextView.length()>0){
                    int_sortorder = Integer.parseInt(mSortOrderTextView.getText().toString());
                } else {
                    int_sortorder = 0;
                }

                ContentResolver contentResolver = getActivity().getContentResolver();//khodesh too video 219 daghighe 8:53 mige chon too fragment hastim bayad besh bechasbe va bad oon ro seda bezanim
                ContentValues values = new ContentValues();

                switch (mMode){
                    case EDIT:
                        if (!mNameTextView.getText().toString().equals(tasks.getName())){//inja besh migim ke age in chizi ke too name vared karde ba oon chizi ke dar database has yeki bood ke hichi age nabood be valyes oon ro ezafe kone
                            values.put(TasksContract.Columns.TASKS_NAME , mNameTextView.getText().toString());
                        }
                        if (!mDescriptionTextView.getText().toString().equals(tasks.getDescription())){//inja besh migim ke age in chizi ke too Description vared karde ba oon chizi ke dar database has yeki bood ke hichi age nabood be valyes oon ro ezafe kone
                            values.put(TasksContract.Columns.TASKS_DESCRIPTION , mDescriptionTextView.getText().toString());
                        }
                        if (int_sortorder != tasks.getSortOrder()){//inja ham oon int_sortorder ke bala gereftim ro moghayese mikonim ba data base
                            values.put(TasksContract.Columns.TASKS_SORTORDER , int_sortorder);
                        }
                        if (values.size() > 0 && mNameTextView.length() > 0){//baressi mikonim ke ayya taghiri karde va hamchenin ye vaght esm ro kamel pak nakarde bashe chon zaroori ast
                            Log.d(TAG, "onClick: updating task");
                            contentResolver.update(TasksContract.buildTaskUri(tasks.getId()) , values ,null ,null);//khob inja baressi mikonim ke aya be values chizi ezafe shode ya na age ezafe shode bood yani chizi taghir karde pas oon ro update mikonim va deghat dashte bash ke ba id in karo kardim
                            Toast.makeText(getActivity() , "Apply Changes" , Toast.LENGTH_SHORT).show();
                            if (mSaveListener != null){//nemidoonam vase chi gozashte filme 235 va 236
                                mSaveListener.onSaveClicked();
                            }
                        } else {
                            if (mNameTextView.length() == 0) {
                                Toast.makeText(getActivity(),"Enter the name" , Toast.LENGTH_SHORT).show();//dar fragment ha vase context az getActivity estefade kon (albate az getContext ham shod vali nemidoonam)
                                break;
                            }
                            if (values.size() == 0){
                                Toast.makeText(getActivity(),"No Record Change" , Toast.LENGTH_SHORT).show();//dar fragment ha vase context az getActivity estefade kon (albate az getContext ham shod vali nemidoonam)
                                if (mSaveListener != null){//nemidoonam vase chi gozashte filme 235 va 236
                                    //inja ham hatman bayad bareesi shavad ke null nabashe
                                    mSaveListener.onSaveClicked();
                                }
                            }
                        }
                        break;
                    case ADD:
                        if (mNameTextView.length() >0 ){//chon goftim ke name task zaroori has injoori check mikonim ke aya vared shode ya na
                            Log.d(TAG, "onClick: adding new task");
                            values.put(TasksContract.Columns.TASKS_NAME , mNameTextView.getText().toString());
                            values.put(TasksContract.Columns.TASKS_DESCRIPTION , mDescriptionTextView.getText().toString());//nemidoonam chera bayad toString ro gozasht??
                            values.put(TasksContract.Columns.TASKS_SORTORDER , int_sortorder);//dar put key bayad string bashad vali value mitoone hame chi bashe
                            contentResolver.insert(TasksContract.CONTENT_URI , values);
                            if (mSaveListener != null){//nemidoonam vase chi gozashte filme 235 va 236
                                mSaveListener.onSaveClicked();
                            }
                            add_successfull = true;
                        } else {
                            Toast.makeText(getActivity() , "enter the name" , Toast.LENGTH_LONG).show();//dar fragment ha vase context az getActivity estefade kon (albate az getContext ham shod vali nemidoonam)
                        }
                        if (add_successfull) {
                            Toast.makeText(getActivity() , "adding successfull" , Toast.LENGTH_LONG).show();//dar fragment ha vase context az getActivity estefade kon (albate az getContext ham shod vali nemidoonam)
                        }
                        break;
                }
                Log.d(TAG, "onClick: done editing");
            }
        });
        Log.d(TAG, "onCreateView: exiting");

        return view;
    }
}
