package com.example.ali.tasktimer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ali on 7/24/17.
 *
 * Basic database class for the application
 *
 * the only class that should use this is {@link AppProvider}
 *
 * be khatere hamin in class private package ast
 */



class AppDatabase extends SQLiteOpenHelper{
    private static final String TAG = "AppDatabase";
    public static final String DATABASE_NAME = "TaskTimer.db";
    public static final int DATABASE_VERSION = 1;

    //Implement AppDatabase as a singelton
    private static AppDatabase instance = null;

    //https://barnamenevisan.org/Articles/Article3991.html : vase inke bafahmi chera in consructor ro private tarif karde
    //chon mikhayem faghat ye instance az in class bashe private kardim in ro be in joor class ha singelton migan ke instance ro too khodeshoon tarif mikonim ba methodi faraham mikonim ke baghiey class ha besh dastresi dashte bashan
    private AppDatabase(Context context){//raftam too class SQLiteOpenHelper va constructor oon ro didam ta bebinam chi niyaz dare yadet has ke vaghti ye chiz ro too super migim yani darim be voroodi haye constructo super class migim
        //public SQLiteOpenHelper(Context context, String name, CursorFactory factory, int version) : in constructore SQLiteOpenHelper ast
        super(context , DATABASE_NAME , null , DATABASE_VERSION);
        Log.d(TAG, "AppDatabase: constructor");
    }

    /**
     * get an instance of the app's singelton database helper object
     * @param context the content provider context
     * @return a sqlite database helper object
     */
    static AppDatabase getInstance(Context context){//khob ma az koja bedoonim bayad be voroodi in context bedim ? chon darim azesh ye doone new mikonim ke bayad be oon context bedim pas be onvane voroodi context midim
        if(instance == null ){
            Log.d(TAG, "getInstance: creatin new instance");
            instance = new AppDatabase(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {//Called automaticly when the database is created for the first time. This is where the creation of tables and the initial population of the tables should happen.
        Log.d(TAG, "onCreate: starts");
        String sSQL;
//        sSQL = "CREATE TABLE Tasks (_id INTEGER PRIMARY KEY NOT NULL , Name TEXT NOT NULL , Description TEXT , SortOrder INTEGER);";
        sSQL = "CREATE TABLE " + TasksContract.TABLE_NAME + " ("
                + TasksContract.Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
                + TasksContract.Columns.TASKS_NAME + " TEXT NOT NULL, "
                + TasksContract.Columns.TASKS_DESCRIPTION + " TEXT, "
                + TasksContract.Columns.TASKS_SORTORDER + " INTEGER);";
        Log.d(TAG, sSQL);
        db.execSQL(sSQL);

        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
