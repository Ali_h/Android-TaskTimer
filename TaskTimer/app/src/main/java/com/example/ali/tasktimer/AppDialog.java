package com.example.ali.tasktimer;

import android.app.Dialog;//amma in hamintori compat has nemikhad support benevisi filme 264 ro hatman bebin
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;

//mibinim ke dialog ha baham tafavot daran az jomle rang o in chiza pas in ro mizarim ke yeki shan

/**
 * Created by ali on 8/2/17.
 *
 */

public class AppDialog extends AppCompatDialogFragment{//DialogFragment ro be AppCompatDialogFragment tabdil kardim ta betoonim az method haie pichide Dialog estefade konim
    //A special version of {@link DialogFragment} which uses an {@link AppCompatDialog} in place of a platform-styled dialog.
    //pas age ma ba material design dialog ha moshek dashtim beyad DialogFragment ro be AppCompatDialogFragment tabdil konim vali khoobe ke hamishe code ma comat bashe vase api haye jadidtar
    //filme 264 khyli moheme

    public static final String TAG = "AppDialog";

    /**
     * ma be yek seri constant niyaz darim ke dar bundle oon ha ro begirim
     */
    public static final String DIALOG_ID = "dialog_id";
    public static final String DIALOG_MESSAGE = "dialog_message";
    public static final String DIALOG_POSITIVE_RID = "dialog_positive_rid";
    public static final String DIALOG_NEGATIVE_RID = "dialog_negative_rid";

    /**
     * the dialogs call back interface to notify of user selected results (deletion confirmed ,etc)
     */
    interface DialogEvent {
        void onPositiveDialogResult(int dialogId, Bundle args);//ma momkene ke dar yek activity chandta dialog ro bekhayem run konim vase hamin bayad bedoonim ke kodoom ro run kardim ke ni kar ba yek unique id anjam mishe

        //in ye ticknik e khoob ast ke id mifrestim in kar mese loader has ke besh id midadim
        void onNegativeDialogResult(int dialogId, Bundle args);

        void onDialogCancelled(int dialogId);//mitoonim in tabe ro nanevisim chon in o gozashtim faghat vase log kardan ke bebinim raftare andriod che goone ast
        //faghat bedoon ke age negative button ro bezani ya khareje dialog click koni dar har soorat tahesh onDismiss farakhani mishe
        //va hadafe dg ie ke dashtim vase inke in 3 tabe ro benevisim (dottaye avval zaroori ast)in bood ke neshoon bedim user mitoone vase dialog 3 halat dashte bashe accept kone cancel kone khareje box click kone
    }

    private DialogEvent mDialogEvent;
    //chon inja yek fragment ast bayad dar onAttach be inteface meghdar bedim

    @Override
    public void onAttach(Context context) {//onAttach : added in API level 23 vase zire in bayad ye kare dg kard chon onAttach farakhani nemishe
        //nemidoonam chera too linke amoozeshe khode android voroodi activity migire  --->  https://developer.android.com/reference/android/app/Fragment.html#onAttach(android.app.Activity)
        //in tike cod mese addeditactivity Fragment has vali dar oonja activity gereftim in ro barresi kon

//        Log.d(TAG, "onAttach: entering onattach ,activity is " + context.toString());vase fahmidane farghe context va getActivity vali dotash ye chizi barmigardoone hala khodet tahghigh kon
//        Log.d(TAG, "onAttach: entering onattach ,activity is " + getActivity().toString());

        super.onAttach(context);

        //activitis containig this fragment must implement its interface
        if (!(context instanceof DialogEvent)) {// instanceof : dar vaghe in check mikone ke aya in class interface inja ro implement karde ya na
            throw new ClassCastException(context.toString() + " must implement DialogEvent interface");
        }
        mDialogEvent = (DialogEvent) context;
        Log.d(TAG, "onAttach: rrrrrrrrrrrrrrrrrrrrr" + mDialogEvent.toString());
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach: entering ...");
        super.onDetach();

        //reset the active callbackd interface becuase we dont have an activity any longer
        mDialogEvent = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {//inja ye warning darim darbare @nonNull ke in baes mishe ke null barnagrdoone fek konam darbarash tahghigh kon hala vase inke in warnin az beyn bere nonNull ezafe kardim
        Log.d(TAG, "onCreateDialog: starts");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());//hatman farghe getActiviy ba getContext ro befahm

        final Bundle arguments = getArguments();
        final int dialogId;
        String messageString;
        int positiveStringId;
        int negativeStringId;

        if (arguments != null) {//inja bundle ro migirim
            dialogId = arguments.getInt(DIALOG_ID);
            messageString = arguments.getString(DIALOG_MESSAGE);

            if ((dialogId == 0) || (messageString == null)){
                throw new IllegalArgumentException("DIALOG_ID and/or DIALOG_MESSAGE not present in the bundle");
            }

            positiveStringId = arguments.getInt(DIALOG_POSITIVE_RID);
            if (positiveStringId == 0) {//inja besh migim ke age barat id dar nazar nagerefte shode bood(manzooram hamin R.string.example has chon in ye id barmigardoone)ye chizi defualt bezar
                positiveStringId = R.string.ok;//int barmigardooone
//                Log.d(TAG, "onCreateDialog: iiiiiiiiiiiiiiiiiiiiiiiiiiioooooooooooooooooooooooo " + R.string.ok); int int barmigardoone
            }
            negativeStringId = arguments.getInt(DIALOG_NEGATIVE_RID);
            if (negativeStringId == 0) {
                negativeStringId = R.string.cancel;
            }
        } else {
            throw new IllegalArgumentException("must pass DIALOG_ID and DIALOG_MESSAGE in the bundle");
        }

        builder.setMessage(messageString)
                .setPositiveButton(positiveStringId, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {//i ro be which taghir dadam  : the button that was clicked (ex. BUTTON_POSITIVE) or the position of the item clicked
                        //callbck positive result method
                        if (mDialogEvent != null) {
                            mDialogEvent.onPositiveDialogResult(dialogId, arguments);//chon dialogId ,arguments dar yek anonymous inner class hastan bayad be soorate final tarif shan
                        }
                    }
                })
                .setNegativeButton(negativeStringId, new DialogInterface.OnClickListener() {//in onclick mese hamoon button amma bejaye inke be button refrence dashte bashe be dialog dare
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        //callback negative result method
                        if (mDialogEvent != null) {
                            mDialogEvent.onNegativeDialogResult(dialogId, arguments);
                        }
                    }
                });

        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {//in tabe khode andriod ast dar vaghe fek konam vase ine ke age khareje dialog click kardi che kar kone vali akhares vase inke dialog napadid she tahesh onDismiss run mishe
//        super.onCancel(dialog); ma bayad in super ro pak konim chon rooye onCancel raftam va roosh control no negah dashtam va roosh click kardam ta beram too classesh va didam ke hich kari nemikone in onCancel pas niyazi be super nadarim
        Log.d(TAG, "onCancel: called");
        if (mDialogEvent != null){
            int dialogId = getArguments().getInt(DIALOG_ID);//dg inja nemikhad chek konim ke aslan aya bundle has ya na ( if argument != null) chon zamani ke in fragment ejra mishe mire too oncratedialog va oonja check kardim ke bundle bashe
            //pas age nabashe oonja bemoon error mide va dg inja nemikhad check konim
            mDialogEvent.onDialogCancelled(dialogId);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {//https://developer.android.com/guide/topics/ui/dialogs.html#DismissingADialog
        //in method har vaght ke roo ye oon button haie ke too onCreateDialog besh goftim click she miayd khodesh in tabe ro ejra mikone hatta age me ovverride nakarde bashimesh
        //age ma yek dialog custom sakhtim vase har buttoni ke mikhaye dissmiss she bayad haalla in tabe ro farakhooni koni va besh begi ba dissmiss();
        Log.d(TAG, "onDismiss: called");
        super.onDismiss(dialog);//age in ro pak koni etefagh haye ajibi miyofte masaaln age gooshi ro rotate koni dobare zaher mishe vali jalebish ine ke baz mitooni chizi ro delete koni vali nabayad dar koll in khat pak she
        //age ke dar in tabe kar e khasi nemikhaye bokoni mitooni in ro pak koni in tabe dorost mese onPuse o onRsume o ... inas ke har vaght bash kar dashti mitooni overridesh oon koni va estefade koni azeshoon
    }
}
