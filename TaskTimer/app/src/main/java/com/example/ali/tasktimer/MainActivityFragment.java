package com.example.ali.tasktimer;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.security.InvalidParameterException;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {//chon recycler view dar in fragment neshoon dade mishe pas ma loader manager ro inja mizarim na too activity balke too activity fragment
    //    https://developer.android.com/guide/components/loaders.html in link ro hatman bekhoon
    private static final String TAG = "MainActivityFragment";

    public static final int LOADER_ID = 0;//dar film 220 toziih dade : to identify our loader chon loader manager mitoone manage kone loader haye mokhtalefi ro vali khode loader manager faghat yedoone ast vali mitoone loader haye ziyade ro manage kone
    //pas har loader ye number unique mikhad vase shenasaie

    private CursorRecyclerViewAdapter mAdapter; //add  adapter refrence


    public MainActivityFragment() {
        Log.d(TAG, "MainActivityFragment: called");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {//zamani ke darim az loader estefade mikonim behtar ast instance az loader ro inja besazim ye axs daram ke inha ro tozih dade(charkhe hayate fragment ha)
        Log.d(TAG, "onActivityCreated: starts");
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID , null , this);// Prepare the loader.  Either re-connect with an existing one, or start a new one.
        //initLoader(int id,android.os.Bundle args,LoaderManager.LoaderCallbacks<D> callback)
        //albate man in tabe ro dar onCreateView ham gozashtam vali farghi nadasht vali khodesh goft behtare inja bezarimesh
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: starts");

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        //cod haye payin daghighan mese filck has
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.task_list);//inja id oon recycler view ro midim
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));//besh context ro dadim dar flick this ferestadim vali inja chon fragment has getContext

        mAdapter = new CursorRecyclerViewAdapter(null , (CursorRecyclerViewAdapter.OnTaskClickListener) getContext());//null barmigardoonim chon hanooz data ie nadarim baraye adapter va amma parametre dovvom
        //fek konam jaye getContex beshe getActivity ham gozash
        //nemidoonam che kar karde film 227 ro hatman bebin
        recyclerView.setAdapter(mAdapter);

        Log.d(TAG, "onCreateView: returning");
        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader: starts with id " + id);
        String[] projection = {TasksContract.Columns._ID, TasksContract.Columns.TASKS_NAME, TasksContract.Columns.TASKS_DESCRIPTION, TasksContract.Columns.TASKS_SORTORDER};//vase CursorLoader niyaze
        //projection : the column that we want the fetch from database
        String sortOrder = TasksContract.Columns.TASKS_SORTORDER + "," + TasksContract.Columns.TASKS_NAME + " COLLATE NOCASE";//vase CursorLoader niyaze
        //in ham vase hamoon order by has
        switch (id) {//agar ke loader haye bishtari khastim dar inja dar case vared mikoinm
            case LOADER_ID:
                //dar fragment ha vase context az getActivity estefade kon (albate az getContext ham shod vali nemidoonam)
                return new CursorLoader(getActivity(),//yek context
                        TasksContract.CONTENT_URI,//uri of the task table
                        projection,//list of the coloumn that we want
                        null,//we want to display all of record that is 2 parametre is null (selecion and selection args)
                        null,
                        sortOrder);//order by
            /**---->MOHEM : khob ye nokte jaleb ke has ine ke dar android monitor ghable hame chi ke neveshte ye chand ta adad has ke dotash ba - az ham joda shodan va mohem hastan
             * hal in dota chi an masalan darim : 4447-4447/? D/MainActivityFragment: onCreateView: starts inja ma log gozahte boodim khob adade avval  process id has va dovvomi thread id has
             * moshahede mikoinm ke hame log ha in do addad yeksan ast amma male query ha fargh mikone thread id ishoon chon loader goftim ke mire va data haye database ro roo ye thread dg ejra mikoe
             * 4447-4463/? D/AppProvider: query: called with URI content://com.example.ali.tasktimer.provider/Tasks moshahede mikonim ke adade thread fargh dare
             * khodesh mige momkene ke bazi az log haye dg thread id fargh dashte bashe vali oona mohem nis chon khode android oon ha ro anjam mide vase ma codie ke neveshtim va rooye thread dg anjam mishe jaleb ast
             */
            //age in ro ba hamin loag cat ha run koni mibini ke bad inja rafte too query chon in CursorLoader khodesh ye query run mikone
            //A CursorLoader to load data backed by a ContentProvider : CursorLoader(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
            //argument haie ke be CursorLoader midim mese hamoon query has ke midadim dar data base va tajob ham nadare chon CursorLoader yek query vase ma run mikone amma dar background thread

            default:
                throw new InvalidParameterException(TAG + "oncreateloader called with invalid loader id " + id);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {//vaghti kare onCreateLoader tamam shod va Loader<Cursor> ro return kard bad miyad too in tabe
        //Loader: The Loader that has finished.        data	: The data generated by the Loader.
        //https://developer.android.com/reference/android/app/LoaderManager.LoaderCallbacks.html#onLoadFinished(android.content.Loader<D>, D)
        Log.d(TAG, "onLoadFinished: starts");
        mAdapter.swapCursor(data);
        int count = mAdapter.getItemCount();

/
        Log.d(TAG, "onLoadFinished: count is : " + count);

        //chizi ke inja mohem ast in ast ke nabayad coursor ro close kard chon in cursor male ma nis male loader ast va age in karo bekonim moshkel be vojood miyad
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {//zamani ejra mishe ke activity baste she masalan berim be ye activity dg va oon ghabliey puse mishe dg ya kollan az barname back bezanim
        Log.d(TAG, "onLoaderReset: starts");
        mAdapter.swapCursor(null);

    }
}
