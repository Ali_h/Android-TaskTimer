package com.example.ali.tasktimer;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by ali on 7/29/17.
 */

class CursorRecyclerViewAdapter extends RecyclerView.Adapter<CursorRecyclerViewAdapter.TaskViewHolder> {//age beri too app flick ham haminjoori neveshtam
    private static final String TAG = "CursorRecyclerViewAdapt";

    private Cursor mCursor;//mese filck oonja yek arraye az image tarif kardim chon dade hamoon image bood vali inja chon dar cursor ast az cursor sakhtim va bad midimesh be constructor

    private OnTaskClickListener mListener;

    interface OnTaskClickListener {//in ro sakhtam ta too onclick azesh estefade konam
        void onEditClick(Tasks tasks);
        void onDeleteClick(Tasks tasks);
    }

    public CursorRecyclerViewAdapter(Cursor mCursor , OnTaskClickListener listener) {
        Log.d(TAG, "CursorRecyclerViewAdapter: called");
        this.mCursor = mCursor;
        mListener = listener;
    }

    //in adapter chizi darbare cursor nemidoone pas methodi nadare ke ba oon moghabele kone ke oon method ro bayad khodemoon ezafe konim oon swapCursor ast

    /**
     *Swap in a new Cursor, returning the old Cursor.
     * the returned old Cursor is <em>not</em> closed.
     *
     * @param newCursor The new cursor to be used.
     * @return Returns the previously set Cursor, or null if there wasn't one.
     * If the given new Cursor is the same instance is the previously set Cursor, null is also returned.
     */
    Cursor swapCursor(Cursor newCursor){//khob in class ro neveshtim ke ma cursor ro close nakonim khdoesh in karo mikone faghat ma olsCursor ro moshakhas mikonim
        Log.d(TAG, "swapCursor: called");
        if (newCursor == mCursor){
            return null;
        }

        final Cursor oldCursor = mCursor;
        mCursor = newCursor;
        if(newCursor!=null){//age newCursor chizi toosh bood pas bayad data set change beshe
            //notify the observes the new cursor
            notifyDataSetChanged();//ba control + q bebin chi mige
        } else{//age ham khali bood bayad az item o ta oonjaye ke has remove she ta khode loader tasmim begire ke close kone
            //notify the observes about the lack of a data set
            notifyItemRangeRemoved(0 , getItemCount());//(int positionStart, int itemCount)
        }
        return oldCursor;
    }

    static class TaskViewHolder extends RecyclerView.ViewHolder{//too flick ham hamintore
        private static final String TAG = "TaTaskViewHolder";

        TextView name = null;
        TextView description = null;
        ImageButton editButton = null;
        ImageButton deleteButton = null;

        public TaskViewHolder (View itemView) {//mese flickr baghie dade ha ro dar voroodi pak mikonim (dalilesh ro nemidoonam)
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.tli_name);//chon innder class ast  itemView bayad avvalesh bashe
            this.description = (TextView) itemView.findViewById(R.id.tli_description);
            this.editButton = (ImageButton) itemView.findViewById(R.id.til_edit);
            this.deleteButton = (ImageButton) itemView.findViewById(R.id.til_delete);
        }
    }

    @Override
    public CursorRecyclerViewAdapter.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {//when a need s a new view
        //in viewType vase in estefade mishe ke age bekhaye har row az recycler view ye chizi neshoon bede ino hatman darbarash tahghigh kon vali age bekhyem ye chizi sabet ro neshoon bedim mitoonim nadide begirimesh(recycler view.get item view type vase zamani ke khasti chizi mokhtalef neshoon bedi)
        //https://stackoverflow.com/questions/26245139/how-to-create-recyclerview-with-multiple-view-type  in link vase hamin ast ke betooni chandat view dashte bashi
        //in tabe chon zamani farakhani mishe ke va view jadid mikhayem dar recycler pas bayad bere az layout va faghat oon ro inflate kone
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_item, parent ,false);
        return new TaskViewHolder(view);
        //in tabe daghighan mese filckr has va bayad ham hamintor bashe mage in ke bekhaye da har satr ye chi neshoon bedi
    }

    //majara intor ast ke recycler view mikhad pas mire be onCreateViewHolder bad in tabe view ro barmigardoone va mire be onBindViewHolder va in miyad view ro neshoon mide
    @Override
    public void onBindViewHolder(CursorRecyclerViewAdapter.TaskViewHolder holder, int position) {//when the recycler view wants a view to display
        //int position ke dar voroodi has oon item ie has ke mikhad bind she yani mikhad neshoon dade beshe
        Log.d(TAG, "onBindViewHolder: starts");

        if ((mCursor == null) || (mCursor.getCount() == 0 )){//khob dar inja barresi mikonim ke aya taski vase namayesh has ya na ke dar inja nis
            Log.d(TAG, "onBindViewHolder: providing instructions");
            holder.name.setText(R.string.instruction_heading);//ye string inja bood ba alt+ enter borademsh too pooshe String
            holder.description.setText(R.string.instructions);//ye string inja bood ba alt+ enter borademsh too pooshe String
            holder.editButton.setVisibility(View.GONE);//ba in kar image button ro invisible mikonim
            holder.deleteButton.setVisibility(View.GONE);//mishe ham nevesht View.INVISIBLE vali View.GONE hengame nemayesh fazaie dar safhe eshghal nemikone vali invisible mikone
        } else{
            if (!mCursor.moveToPosition(position)){//ino nemidoonam vase chi gozashte
                throw new IllegalStateException("couldent move the cursor to position : " + position);
            }
            final Tasks tasks = new Tasks(mCursor.getLong(mCursor.getColumnIndex(TasksContract.Columns._ID)),//in ro final kardam chon mikhayem besh too onclick ke inner class has dastresi dashte bashim
                                    //getLong() : long getLong (int columnIndex)
                                    mCursor.getString(mCursor.getColumnIndex(TasksContract.Columns.TASKS_NAME)),
                                    mCursor.getString(mCursor.getColumnIndex(TasksContract.Columns.TASKS_DESCRIPTION)),
                                    mCursor.getInt(mCursor.getColumnIndex(TasksContract.Columns.TASKS_SORTORDER)));

            //tafavoti ke ba flick has dar injas dar flickr chon ma ye array az image dashtim in ja migoftim Photo photoItem = mPhotosList.get(position); vali inja chon cursor has lazem nis begim
            holder.name.setText(tasks.getName());//int getColumnIndex (String columnName) : return : int the zero-based column index for the given column name, or -1 if the column name does not exist.
            /**ye tozih dar rabete ba khate bala : bebin ma mikhayem namr ro neshoon bedim pas goftim ke mCursor.getString() ta injash ke hichi chon getString() vase ma ye string ke hamooon name has ro barmigardoone
             * hal chon dar database hastim in getString() intor kar mikone ke bayad shomare column ie ke mikhaye neshoon bedi ro be onvane voroodi besh bedi yani bayad be getString() begi ke kodoom sotoon made nazaret has
             * ke goftim mCursor.getColumnIndex(TasksContract.Columns.TASKS_NAME)) : ke avval mige getColumnIndex() in hamoon index sotoon ro vase ma barmigardoone amma az koja bedoone che sotooni ke vazife
             * ma in ast ke besh be onvane voroosi esme sotoon ro be hala string besh begim
             */
            holder.description.setText(tasks.getDescription());
            holder.editButton.setVisibility(View.VISIBLE);// TODO ad onclick listener
            holder.deleteButton.setVisibility(View.VISIBLE);// TODO ad onclick listener


            /**
             * khob inja jaie ast ke bayad onclick ro bezarim amma be ye chiz deghat dashte bash inke ma yek recycler darim ka masalan 7 ta data tooshe bad in 7 ta data imgae button daran ke vase har
             * 7 tashoon id inha yeki ast pas avvalan ke tashkhis dadane inke kodoom zade shode sakht ast va hamchenin age in karo inja anjam bedim momkene beshe ba zerangi kardan toosh bug gozasht
             * hamoontor ke too flick raaftim vase recycler class onClick neveshtim vase hamin bood ke tashkhis bedim ke kodoom zade shode ast amma dar in app mikhayem yek modele dg yad begirim va oon ham
             * CALL BACK ast vase inke bafahmi in che kar mikone code khate 160 ro neveshtam ooomadm anonymous class ro yek class kardam va base inke class Button (hamoon til_edit va delete az class Button hastan dg)
             * .setonclicklistener esh kar kone ye instance az class sakhtam va besh dadam amma bayad az onclick ham implement mikardam alan call back yani in :
             * ma yek object az class listener sakhtim ke toosh yek methode onClick has va oon object ro pass dadim be class Button ba estefade az setOnclicklistener hala Button yek refrence dare az daghighan
             * hamoon object ke hala Button midoone ke masaalan dare in button click mishe va midoone ke daghighan kodoom has chon yek refrence dare
             * pas intor shod: alan mikham Button ro tozih bedam : ma yek recycler darim ke har koddom onbind shode va har kodoom onclick dare va hame imgaebutton ha ideshoon yeki ast hal mikhayem
             * intor bashe ke vaghti rooye kodoom az data ha click shod va be button setonclick dadim bedoone kodoom boode(baz ham migam dar sharayete adi ham mifahme amma momkenen moshkel pish biyad)
             * pas ma yek class sakhtim listener ke azesh ye object sakhtim va oon object ro dadim be class Button alan Button az har object ye refrence dare pas kar ma tamoom shod
             * hala mikhayem kooli in masale ro nega konim ma darim be har kodoom az row ha onbind midim va mikhayem ke har kodoom edit va delete khodeshoon ro dashte bashan pas bayad har kodooom az in row ha
             * be ye class refrence dade beshe ke oon class main activity has amma chera main fragment nemitoone bashe chon ma too main activity be addedit activity dastresi darim vali fragment chizi darbare in
             * nemiidoone va mese inke khoob ham nis ke az yek fragment yek fragment dg ro run kard pas bayad too main activity neveshte beshe
             * loade manager ham hamintor ast age bebini mainfragment implement karde loadermanger.loadecallback ro
             * amma dar inja chenin mishe : Button calls back this cursorrecyclerviewadapter class when the button tapped and this class call back the main activity and passing the task nedds be edit or delete
             * so in the way activity take care of editing or deleting
             * bebin pas kollan call back ine ke yek object az yek class ro bedi be ye class dg
             * filme 226 ta 228 mohemme
             * vali be nazare man mishe too hamin onclick karesh ro kard dg nakhayem befrestim main faghat har chi khastim mizani view.getContex bad har chi khasti
             */
            View.OnClickListener buttonListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: starts");
                    switch (view.getId()){
                        case R.id.til_edit:
                            if (mListener != null){
                                mListener.onEditClick(tasks);
                            }
                            break;
                        case R.id.til_delete:
                            if (mListener != null){
                                mListener.onDeleteClick(tasks);
                            }
                            break;
                        default:
                            Log.d(TAG, "onClick: found unexepted button id");
                    }
                }
            };
            //vaghti ke ye click zade mishe ro button ha onclicklistener attach to the Button va badesh ye call back midim be mainactivity
            holder.editButton.setOnClickListener(buttonListener);
            holder.deleteButton.setOnClickListener(buttonListener);

//
//            /**
//             * in payin khyli mohem ast
//             */
//
//            //dar in marhale mikhayem onclick bezarim ta image button ha ye kari bekonan
////            View.OnClickListener buttonListener = new View.OnClickListener() {//baz ham migam in chon yek inner class ast faghat be final ha az biroon dastresi darad va toosh vase context this nemitooni bedi
//            class Listener implements View.OnClickListener{//ma khat bala ke yek anonymous inner class bood ro be yek class tabdil kardim ke dg anonymous nis amma be che soorat
//                //dar khate 127-8 setOnClickListener entezar dare ke buttonListener onclick method ro dashte bashe amma age zamani ke implements nakarde bashim ba inke method onClik ro neveshim amma
//                //compiler java into nemifahme compiler bayad motmaen she ke ma in method ro hatman minevisim va tanha rahi ke mishe be compiler esbat kard ke ma in karo mikonim va yek onClik be buttonListener
//                //midim ine ke az View.OnClickListener implements konim hal vaghti implements mikonim compiler motmaen mishe ke hame method ha ro too oon interface ma neveshtim va dorost ham hastan
//                //age ham bebini faghat az View.OnClickListener faghat onCLick has ke mishe azesh implement kard
//                @Override
//                public void onClick(View view) {
//                    Log.d(TAG, "onClick: starts");
//                    Log.d(TAG, "onClick: button with id : " + view.getId() + " is tapped");
//                    Log.d(TAG, "onClick: task name is :" + tasks.getName());
//                }
//            }
//            Listener buttonListener = new Listener();
//            holder.editButton.setOnClickListener(buttonListener);
//            holder.deleteButton.setOnClickListener(buttonListener);

        }

    }

    @Override
    public int getItemCount() {//recycler view uses that to how many item there are to display
        if ((mCursor == null) || (mCursor.getCount()==0 )){
            return 1 ;//becuase we populated a single viewholder  with instruction
            //1 barmigardoonim chon age hichi nabood mikhayem ye instruction neshoon bedim
        }else{
            return mCursor.getCount();
        }
    }
}
