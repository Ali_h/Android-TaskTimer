package com.example.ali.tasktimer;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import static com.example.ali.tasktimer.AppProvider.CONTENT_AUTHORITY;
import static com.example.ali.tasktimer.AppProvider.CONTENT_AUTHORITY_URI;

/**
 * Created by ali on 7/24/17.
 */

public class TasksContract{
    private static final String TAG = "TasksContract";

    static final String TABLE_NAME = "Tasks";

    public static class Columns {//in class static ast chon mikhayem bedoone sakhte instance azesh be fiel hash dastresi dashte bashim
        //chera kollan vase column ha class sakhtim ? chon dar inja chandta table darim mikhayem ghati nashe masalan vase in darim TasksContract._ID
        //ke mifahmim in vase table task has
        //in payin name column ha ro dadim
        public static final String _ID = BaseColumns._ID;
        public static final String TASKS_NAME = "Name";
        public static final String TASKS_DESCRIPTION = "Description";
        public static final String TASKS_SORTORDER = "SortOrder";

        private Columns(){
            //private constructor to perevent instantiation
        }
    }

    //other classess can use to get the uri of tasks table
    //the uri too access the task table
    //dar asl in miyad CONTENT_AUTHORITY_URI ke az AppProvider miyad ro migire va esme table khodesh ro mizare akharesh va baghi ja ha mitoonan az in uri estefade konan
    public static final Uri CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI , TABLE_NAME);//withAppendedPath : Creates a new Uri va fek konam ham khodesh content:// avvalesh mizare

    static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;
    static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;

    static  Uri buildTaskUri(long taskId){//dar in tabe ma besh id ro midim ta ezafe kone
        return ContentUris.withAppendedId(CONTENT_URI , taskId);//withAppendedId : Appends the given ID to the end of the path.
    }

    static long getTaskId(Uri uri){//va dar inja ham id ro migirim
        //chon momkene tedad id ha khyli khyil ziyad bashe vase hamin long barmigardoonim
        return ContentUris.parseId(uri);//parseId : Converts the last path segment to a long.ID is stored in the last segment.
    }
}

