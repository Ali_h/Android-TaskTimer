package com.example.ali.tasktimer;

import java.io.Serializable;

/**
 * Created by ali on 7/28/17.
 *
 * package private
 */

//vase in Serializable kardim vase inke too intent.putextra betoonim befrestimesh
class Tasks implements Serializable {//nemidoonam chera inja ro Serializable kard vali mige vase inke ma mikhayem ye instance az in class ro be bundle ha bedim
    //Serializable hich class ya methodi nadarad faghat vase moshakhas kardan ast
    public static final long serialVersionUID = 20161120L;//dar flickr ham in serail ro barresi kardim oonja faght ye 1 dadim vali inja tarikh ro dadim

    private long m_Id;//khob in field ro final nakardim chon mige ke (dar film 215) ma niyaz darim ke in field ro update konim

    //in 3 ta field ro final kardim chon ma nemikhayem ke dade haye data base avaz she ke ya betoonim barashoon setter tarif konim
    private final String mName;
    private final String mDescription;
    private final int mSortOrder;

    public Tasks(long id, String mName, String mDescription, int mSortOrder) {
        this.m_Id = id;
        this.mName = mName;
        this.mDescription = mDescription;
        this.mSortOrder = mSortOrder;
    }

    public long getId() {
        return m_Id;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getSortOrder() {
        return mSortOrder;
    }

    public void setId(long id) {
        this.m_Id = id;
    }

    @Override
    public String toString() {
        return "Tasks{" +
                "m_Id=" + m_Id +
                ", mName='" + mName + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mSortOrder=" + mSortOrder +
                '}';
    }
}
