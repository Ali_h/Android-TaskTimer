package com.example.ali.tasktimer;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by ali on 7/25/17.
 *
 * Provider for the TaskTimer app . this is the only class that knows about {@link AppDatabase}
 */

//https://developer.android.com/guide/topics/providers/content-provider-creating.html baraye sakhte content provider va az in ghesmat bekhoon : Designing Content URIs
public class AppProvider extends ContentProvider{
    private static final String TAG = "AppProvider";

    private AppDatabase mOpenHelper;

    //https://developer.android.com/reference/android/content/UriMatcher.html baraye uri matcher khoob ast
    private static final UriMatcher sUriMatcher = buildUriMatcher();//darbare in khyli tahgigh kon

    static final String CONTENT_AUTHORITY = "com.example.ali.tasktimer.provider";//To avoid conflicts with other providers, you should use Internet domain ownership (in reverse) as the basis of your provider authority
    // if your Android package name is com.example.<appname>, you should give your provider the authority com.example.<appname>.provider.

    public static final Uri CONTENT_AUTHORITY_URI = Uri.parse("content://" + CONTENT_AUTHORITY); //dar task timer helper(ke to khoode pooshe has) dar ghesmate 6 tozih dadam in bakhsh ro
    //public goftim chon mikhayem dar kharej inja besh dastresi dashte bashim age yadet bashe too barname content provide example oonja ke goftim contact contract oon ham ye class bood mese hamin task contract
    //ke uri khodesh ro dasht va ma bejaye inke bedoonim uri oon chi bood az hamin fieldesh ke public tarif shode bood estefade kardim hal ma ham ino public mikonim ta dg nakhayem bedoonim uri chi boode
    //faghat benevisim CONTENT_AUTHORITY_URI
    //faghat in bayad public bashe chon azesh dar jaha ye dg estefade mikonim

    private static final int TASKS = 100;
    private static final int TASKS_ID = 101;

    private static final int TIMINGS = 200;
    private static final int TIMINGS_ID = 201;

//    private static final int TASK_TIMINGS = 300;
//    private static final int TASK_TIMINGS_ID = 301;

    private static final int TASK_DURATIONS = 400;
    private static final int TASK_DURATIONS_ID = 401;

    private static UriMatcher buildUriMatcher(){//khob in tabe vase ine ke ma sUriMatcher ro UriMatcher tarif kardim va miyaem too in tabe besh ye seri Uri ezafe mikonim(ke bishtare oghat hamoon table ha hastan ba id hashoon) bad vase har method ke khsatim ejra konim oon ro ba uri asli match mikonim ke bebinim kodoomeshoon ast
        //bad har kodoom ke bood adade oon ro barmigardoone --> bebin be soorate kolli injoorie ke ma ye sUriMatcher darim ke hamoon table ha ba id hashoon hastan va bad bayad oon ha ro vase har kar ba uri asli moghayese bokonim va bebinim ke kodoom hastan
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);//agar table name daroon uri nabashad no_match ro barmigardoone

        //content://com.example.ali.tasktimer.provider/Tasks
        matcher.addURI(CONTENT_AUTHORITY , TasksContract.TABLE_NAME , TASKS);//void addURI (String authority,String path,int code)
        //agar table tasks id nadashte bashad tasks ro barmigardoone
        //content://com.example.ali.tasktimer.provider/Tasks/8
        matcher.addURI(CONTENT_AUTHORITY , TasksContract.TABLE_NAME + "/#" , TASKS_ID);//fek konam zamani ke mizanim buildTaskUri(2) in chon too parantez ye id gofte bayad in bashe yani bayad in bashe ta betoonim barash in tabe ro faraham konim
        //yani age in nabashe in tabe ham kar nemikone
        //agar table tasks id ro faraham karde bashad TASKS_ID ro barmigardoone

//        matcher.addURI(CONTENT_AUTHORITY , TimingsContract.TABLE_NAME , TIMINGS);
//        matcher.addURI(CONTENT_AUTHORITY , TimingsContract.TABLE_NAME + "/#" , TIMINGS_ID);
//
//        matcher.addURI(CONTENT_AUTHORITY , DurationsContract.TABLE_NAME , TASK_DURATIONS);
//        matcher.addURI(CONTENT_AUTHORITY , DurationsContract.TABLE_NAME + "/# ", TASK_DURATIONS_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {//https://developer.android.com/guide/topics/providers/content-provider-creating.html
        mOpenHelper = AppDatabase.getInstance(getContext());//be khatere inke AppDatabase.getInstance zadim chon in singelton ast va nesmihe oon ro new kard
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        //projection : containing the name column
        //sortOrder : How the rows in the cursor should be sorted and containing the name of row with comma mese hamoon ORDER BY dar sql ast
        //https://developer.android.com/reference/android/content/ContentProvider.html#query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String) baraye query khoob ast
        Log.d(TAG, "query: called with URI " + uri);
        final int match = sUriMatcher.match(uri);//int match(uri)
        Log.d(TAG, "query: match is " + match);

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();//This is a convience class that helps build SQL queries to be sent to SQLiteDatabase objects.

        switch (match){
            case TASKS:
                queryBuilder.setTables(TasksContract.TABLE_NAME);
                break;
            case TASKS_ID:
                queryBuilder.setTables(TasksContract.TABLE_NAME);//setTables : be zabane sade mige ke az kodoom table query mikhad
                long taskId = TasksContract.getTaskId(uri);
                queryBuilder.appendWhere(TasksContract.Columns._ID + " = " + taskId);//appendWhere : hamoon where dar sql ast
                break;

//            case TIMINGS:
//                queryBuilder.setTables(TimingsContract.TABLE_NAME);
//                break;
//            case TIMINGS_ID:
//                queryBuilder.setTables(TimingsContract.TABLE_NAME);
//                long timingId = TimingsContract.getTimingId(uri);
//                queryBuilder.appendWhere(TimingsContract.Columns._ID + " = " + timingId);
//                break;
//
//            case TASK_DURATIONS:
//                queryBuilder.setTables(DurationsContract.TABLE_NAME);
//                break;
//            case TASK_DURATIONS_ID:
//                queryBuilder.setTables(DurationsContract.TABLE_NAME);
//                long durationId = DurationsContract.getDurationId(uri);
//                queryBuilder.appendWhere(DurationsContract.Columns._ID + " = " + durationId);
//                break;

            default:
                throw new IllegalArgumentException("Unknown uri " + uri);//IllegalArgumentException : You may choose to throw this if your provider receives an invalid content URI
        }

        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        
        /**khob alan ye moshkel darim inke vaghti data add mikonim age back ro bezanim be recycler ezafe shode vali age up button ro bezanim ezafe nemishe yani ma bayad vase
         *har kari ke database ro taghir mide ye notification bezanim ke data ha avaz shode
         */

        Cursor cursor = queryBuilder.query(db , projection , selection, selectionArgs ,null , null , sortOrder); // ba control + q rooye query bebin che kar mikone in karo hatman bekon

        Log.d(TAG, "query: row in returning cursor : " + cursor.getCount());

        cursor.setNotificationUri(getContext().getContentResolver() ,uri);//setNotificationUri hatman darbarash search kon
        //dar vaghe in miyad dobare query ro ejra mikone har vaght barmigardi be main activity fragment ke bekhaye data ha ro bebini dobare in baes mishe ke query run she
        //albate vase hamashoon yani ham insert ham delete ham update in karo mikone

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {//in tabe ro dar net search kon---> CONTENT_TYPE CONTENT_ITEM_TYPE : in dota ro ke dar TasksContract neveshtim vase in mime type ha has
        final int match = sUriMatcher.match(uri);

        switch (match){
            case TASKS:
                return TasksContract.CONTENT_TYPE;

            case TASKS_ID:
                return TasksContract.CONTENT_ITEM_TYPE;//inja item goftim chon age yadet bashe goftim in tabe vase id has va chon id ye doone has vase hamin mishe ye doone item

//            case TIMINGS:
//                return TimingsContract.CONTENT_TYPE;
//
//            case TIMINGS_ID:
//                return TimingsContract.CONTENT_ITEM_TYPE;
//
//            case TASK_DURATIONS:
//                return DurationsContract.CONTENT_TYPE;
//
//            case TASK_DURATIONS_ID:
//                return DurationsContract.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalArgumentException("Unknown uri " + uri);//IllegalArgumentException : You may choose to throw this if your provider receives an invalid content URI
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {//Implement this to handle requests to insert a new row
        //ContentValues: A set of column_name/value pairs to add to the database. This must not be null.
        Log.d(TAG, "Entering insert called with uri : " + uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG, "match is : " + match);

        final SQLiteDatabase db;//dalilesh ro payin goftam ke chera inja db ro meghdar dahi nakardim ba getWritableDatabase

        Uri returnUri;
        long recordId;//long insert() : in tabe ke payin tarif kardim id oon row ke ezafe shode ro barmigardoone

        switch (match){
            case TASKS:
                db = mOpenHelper.getWritableDatabase();//getWritableDatabase va getReadableDatabase method ye khorde kond hastan va ma ham nemikhayem doori konim az inke oon haro seda bezanim dar hali ke uri invalide vared shode ast
                //pas oon ro bad switch seda mizanim va age uri invalid bashe exeception throw mishe va dg getWritableDatabase sakhte nashode ke bekhad kond bashe va ma in do mehod ro zamani misazim ke uri dorost bashe yani daroon switch
                recordId = db.insert(TasksContract.TABLE_NAME , null ,values);//ba control + q bebin insert voroodi chi migire hatman bebin
                if (recordId >= 0 ){//insert age error dashte bashe -1 barmigardoone va ma inaj check mikonim ke -1 nabashe
                    returnUri = TasksContract.buildTaskUri(recordId);//buildTaskUri : dar TaskContract ke neveshtim in tabe ro voroodish ye long bod ke vase hamin recordId long has va dalile dgash ham vase ine ke insert long barmigardoone
                } else{
                    throw new android.database.SQLException("failed to insert into " + uri.toString());
                }
                break;

            case TIMINGS:
//                db = mOpenHelper.getWritableDatabase();
//                recordId = db.insert(TimingsContract.Timings.buildTimingsUri(recordId));
//                if (recordId >= 0){
//                    returnUri = TimingsContract.Timings.buildTimingsUri(recordId);
//                }else {
//                    throw new android.database.SQLException("failed to insert into " + uri.toString());
//                }
//                break;

            default:
                throw new IllegalArgumentException("Unknown uri : " + uri);
        }
        if (recordId >= 0){//inja motmaen shodim ke data insert shode bad notify change dadim
            //some thing was changed
            Log.d(TAG, "insert: setting notify changed with : " + uri);
            getContext().getContentResolver().notifyChange(uri ,null);//notifyChange : Notify registered observers that a row was updated and attempt to sync changes to the network.
            //mese inke khodesh mige voroodi dovvom hamishe null has film 224
        } else {
            //noting change
            Log.d(TAG, "insert: nothing inserted");
        }
        Log.d(TAG, "exiting insert returning " + returnUri);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {//delete values be onvane voroodi nemigire chon manteghi ham has faghat mikhad delete kone va faghat where lazem dare
        //in int ke barmigardoone hamoon tedad khoonie has ke delete shode
        Log.d(TAG, "Entering delete called with uri : " + uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG, "match is : " + match);

//        Log.d(TAG, "update: ddddddddddddddddddddddddddddddddddddd" + selection.toString());
//        Log.d(TAG, "update: ssssssssssssssssssssssssssssssssssssss" + selectionArgs.toString());

        final SQLiteDatabase db ;
        int count;//vase inro tarif kardim chon dar db.delete yek int barmigardoone ke hamoon tedad row haye update shode ast

        String selectionCriteria;//ma ino sakhtim vase inke dar case TASKS ma mikhayem ye delete kolli dashte bashim(amma yek doonie ham mishe) be din soorat ke masalan mige name = "ali" vali id ro nemige
        //chon age id bege mire case bad chon age id bege pas hatman mikhad yedoone row ro delete kone pas ma inro misazim va beshi migiim ke id to chi has

        switch (match){
            case TASKS:
                db = mOpenHelper.getWritableDatabase();
                count = db.delete(TasksContract.TABLE_NAME , selection , selectionArgs);//ba control + q bebin hatamn update ro
                //update() : return int the number of rows affected
                //albate ye moshkel has az android studio ke return update bayad long bashe mese insert na int
                break;

            case TASKS_ID:
                db = mOpenHelper.getWritableDatabase();
                long taskId = TasksContract.getTaskId(uri);//khob inja ma id ro migirim be vasile in tabe getTaskId() ke dar TaskContract neveshtim
                selectionCriteria = TasksContract.Columns._ID + " = " + taskId;

                if ((selection != null) && (selection.length() > 0)){//khob momkene ke user id ro bede ke vared e in case mishe vali momkene ke yek condition ezafe ham gofte bashe ke in mishe hamoon selection pas ma check mikonim ke age selection null nabood va ye chizi dasht oon ro be selectionCriteria ezafe mikonim
                    //hamoon tor ke bala ham goftam selection mese hamoon where has vali age karbar khast ye doone row ro update kone ba id miyad inja va selectionCriteria ro vase hamin sakhtim ke id ro begire va age selection ham bood oon ro be selectionCriteria ezafe mikonim
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.delete(TasksContract.TABLE_NAME ,selectionCriteria ,selectionArgs);
                break;

//            case TIMINGS:
//                db = mOpenHelper.getWritableDatabase();
//                count = db.delete(TimingsContract.TABLE_NAME ,selection , selectionArgs);//ba control + q bebin hatamn delete ro
//                //delete() : return int the number of rows affected
//                //albate ye moshkel has az android studio ke return update bayad long bashe mese insert na int
//                break;
//
//            case TIMINGS_ID:
//                db = mOpenHelper.getWritableDatabase();
//                long timingsId = TimingsContract.getTaskId(uri);//khob inja ma id ro migirim be vasile in tabe getTaskId() ke dar TimingsContract neveshtim
//                selectionCriteria = TimingsContract.Columns._ID + " = " + timingsId;
//
//                if ((selection != null) && (selection.length() > 0)){//khob momkene ke user id ro bede ke vared e in case mishe vali momkene ke yek condition ezafe ham gofte bashe ke in mishe hamoon selection pas ma check mikonim ke age selection null nabood va ye chizi dasht oon ro be selectionCriteria ezafe mikonim
//                    //hamoon tor ke bala ham goftam selection mese hamoon where has vali age karbar khast ye doone row ro update kone ba id miyad inja va selectionCriteria ro vase hamin sakhtim ke id ro begire va age selection ham bood oon ro be selectionCriteria ezafe mikonim
//                    selectionCriteria += " AND (" + selection + ")";
//                }
//                count = db.delete(TimingsContract.TABLE_NAME ,selectionCriteria ,selectionArgs);
//                break;

            default:
                throw new IllegalArgumentException("unknown uri : " + uri);
        }

        if (count > 0) {//check kardim ke hatman chizi pak shode bashad bad goftim ke change kon
            //khob ma bayad inkar ha ro bekonim chon vaghti database update mishe ba in kar be cursor loader migim ke database avaz shode(fillm 224 hatman dide shavad)va oon ham mire bad be on load finish
            //some thing was delete 
            Log.d(TAG, "delete: settin notify changed with : " + uri);
            getContext().getContentResolver().notifyChange(uri ,null);
        } else {
            Log.d(TAG, "delete: notthing delete ");
        }

        Log.d(TAG, "exiting delete retrurning " + count);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {//in int ke barmigardoone hamoon tedad khoonie has ke update shode
        Log.d(TAG, "Entering update called with uri : " + uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG, "match is : " + match);

//        Log.d(TAG, "update: ddddddddddddddddddddddddddddddddddddd" + selection.toString());
//        Log.d(TAG, "update: ssssssssssssssssssssssssssssssssssssss" + selectionArgs.toString());

        final SQLiteDatabase db ;
        int count;//vase inro tarif kardim chon dar db.update yek int barmigardoone ke hamoon tedad row haye update shode ast

        String selectionCriteria;//ma ino sakhtim vase inke dar case TASKS ma mikhayem ye update kolli dashte bashim(amma yek doonie ham mishe) be din soorat ke masalan mige name = "ali" vali id ro nemige
        //chon age id bege mire case bad chon age id bege pas hatman mikhad yedoone row ro update kone pas ma inro misazim va beshi migiim ke id to chi has

        switch (match){
            case TASKS:
                db = mOpenHelper.getWritableDatabase();
                count = db.update(TasksContract.TABLE_NAME , values , selection , selectionArgs);//ba control + q bebin hatamn update ro
                //update() : return int the number of rows affected
                //albate ye moshkel has az android studio ke return update bayad long bashe mese insert na int
                break;

            case TASKS_ID:
                db = mOpenHelper.getWritableDatabase();
                long taskId = TasksContract.getTaskId(uri);//khob inja ma id ro migirim be vasile in tabe getTaskId() ke dar TaskContract neveshtim
                selectionCriteria = TasksContract.Columns._ID + " = " + taskId;

                if ((selection != null) && (selection.length() > 0)){//khob momkene ke user id ro bede ke vared e in case mishe vali momkene ke yek condition ezafe ham gofte bashe ke in mishe hamoon selection pas ma check mikonim ke age selection null nabood va ye chizi dasht oon ro be selectionCriteria ezafe mikonim
                    //hamoon tor ke bala ham goftam selection mese hamoon where has vali age karbar khast ye doone row ro update kone ba id miyad inja va selectionCriteria ro vase hamin sakhtim ke id ro begire va age selection ham bood oon ro be selectionCriteria ezafe mikonim
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.update(TasksContract.TABLE_NAME , values ,selectionCriteria ,selectionArgs);
                break;

//            case TIMINGS:
//                db = mOpenHelper.getWritableDatabase();
//                count = db.update(TimingsContract.TABLE_NAME , values , selection , selectionArgs);//ba control + q bebin hatamn update ro
//                //update() : return int the number of rows affected
//                //albate ye moshkel has az android studio ke return update bayad long bashe mese insert na int
//                break;
//
//            case TIMINGS_ID:
//                db = mOpenHelper.getWritableDatabase();
//                long timingsId = TimingsContract.getTaskId(uri);//khob inja ma id ro migirim be vasile in tabe getTaskId() ke dar TimingsContract neveshtim
//                selectionCriteria = TimingsContract.Columns._ID + " = " + timingsId;
//
//                if ((selection != null) && (selection.length() > 0)){//khob momkene ke user id ro bede ke vared e in case mishe vali momkene ke yek condition ezafe ham gofte bashe ke in mishe hamoon selection pas ma check mikonim ke age selection null nabood va ye chizi dasht oon ro be selectionCriteria ezafe mikonim
//                    //hamoon tor ke bala ham goftam selection mese hamoon where has vali age karbar khast ye doone row ro update kone ba id miyad inja va selectionCriteria ro vase hamin sakhtim ke id ro begire va age selection ham bood oon ro be selectionCriteria ezafe mikonim
//                    selectionCriteria += " AND (" + selection + ")";
//                }
//                count = db.update(TimingsContract.TABLE_NAME , values ,selectionCriteria ,selectionArgs);
//                break;

            default:
                throw new IllegalArgumentException("unknown uri : " + uri);
        }

        if(count > 0){//khob ma bayad inkar ha ro bekonim chon vaghti database update mishe ba in kar be cursor loader migim ke database avaz shode(fillm 224 hatman dide shavad)va oon ham mire bad be on load finish
            //some thing update
            Log.d(TAG, "update: setting notify change with : " + uri);
            getContext().getContentResolver().notifyChange(uri ,null);
        } else{
            //nothing update
            Log.d(TAG, "update: nothin update");
        }
        Log.d(TAG, "exiting update retrurning " + count);
        return count;
    }
}
