package com.example.ali.tasktimer;

import android.support.v7.app.AlertDialog;//inja ham ghablesh support.v7 ro nadasht va about dialog kami motefavet tar bood ba api balaye 21 masalan icon ro bad neshoon midad vase hamin ma in ro ezafe kardim
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements CursorRecyclerViewAdapter.OnTaskClickListener, AddEditActivityFragment.OnSaveClicked ,AppDialog.DialogEvent{//inja az AddEditActivityFragment.OnSaveClicked
    //implement kardim chon zamani ke too landscape bashim fragment too mainActivity neshoon dade mishe va ma bayad inja handle sh konim
    //hamchenin chon dialog ha callback ast az AppDialog.DialogEvent ham implement kardim
    private static final String TAG = "MainActivity";

    private boolean mTwoPane = false;//whether or not the activity is in 2-pane mode  //running in landscape on a tablet

    public static final int DIALOG_ID_DELETE = 1;//vase inke be dialog bekhayem id bedim ,  estefade shode va vase in dialog ast ke mikhayem chizi ro pak koinm
    public static final int DIALOG_ID_CANCEL_EDIT = 2;//vase inke bekhayem az barname bedoon save kardane dade ha berim biroon ast

    private AlertDialog mDialog = null; //module scope becuase we need to dismiss it in onStop
                                        //e.g when oriantion changes to avoid memory leaks
                                        //https://developer.android.com/guide/topics/ui/dialogs.html#CustomLayout   in link ro vase sakhte dialog custom hatman bekhoon
                                        //If you want a custom layout in a dialog, create a layout and add it to an AlertDialog by calling setView() on your AlertDialog.Builder object.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(R.id.task_details_container) != null) {//dar layout land/conten_main ma oomadim barash ye frame layout gozashtim ba in id hala migim ke age in id mojjoood bood oon vaght age khast add ya edit kone barash fragment side by side baz she
            //va inja mTwoPnae ro true kardim ke bad azesh estefade konim chon aval oncreate ejra mishe ma inja check kardim ke aya layout ma in id ro shamel shode ya na ?
            //the detail container view will be present only in the large-screen layouts (res/values-land and res/values-sw600dp)
            //if this view is present then the activity should be in two-pane mode.
            mTwoPane = true;
            findViewById(R.id.task_details_container).setVisibility(View.GONE);
        }

//
//        String[] projection = { TasksContract.Columns._ID,
//                                TasksContract.Columns.TASKS_NAME,
//                                TasksContract.Columns.TASKS_DESCRIPTION,
//                                TasksContract.Columns.TASKS_SORTORDER};//in faghat vase test has
//
//        ContentResolver contentResolver = getContentResolver();
//
//        ContentValues values = new ContentValues();
////        baraye delete kardan ba args baraye jologiri az sqlinject
////        String selection = TasksContract.Columns.TASKS_DESCRIPTION + " = ?";
////        String[] args = {"for sql inject"};
////        int count = contentResolver.delete(TasksContract.CONTENT_URI ,selection ,args);//dar vaghe in voroodi 4 om ke dar delete va update bood (String[] selectionArgs) vase jologiri az sql inject has be soorati ke dar selection ? mizarim va ye arraye besh midim az args
////        Log.d(TAG, "onCreate: " + count + " record(s) deleted");
//
////        baraye delete kardan
////        int count = contentResolver.delete(TasksContract.buildTaskUri(4) ,null,null);//dar vaghe in voroodi 4 om ke dar delete va update bood (String[] selectionArgs) vase jologiri az sql inject has be soorati ke dar selection ? mizarim va ye arraye besh midim az args
////        Log.d(TAG, "onCreate: " + count + " record(s) deleted");
//
////        baraye jologiri az sql inject args ham midim
////        values.put(TasksContract.Columns.TASKS_DESCRIPTION , "for sql inject");
////        String selection = TasksContract.Columns.TASKS_SORTORDER + " = ?";
////        String[] args = {"3"};
////        int count = contentResolver.update(TasksContract.CONTENT_URI , values ,selection ,args);//dar vaghe in voroodi 4 om ke dar delete va update bood (String[] selectionArgs) vase jologiri az sql inject has be soorati ke dar selection ? mizarim va ye arraye besh midim az args
////        Log.d(TAG, "onCreate: " + count + " record(s) upgraded");
//
////        vase chand ta update kardan
////        mibinim ke dg besh TasksContract.buildTaskUri(5) nadadim chon in vase faghat ye doone row has besh khdoe uri ro dadim va ye selection ke hamoon where ast
////        values.put(TasksContract.Columns.TASKS_SORTORDER , "3");
////        values.put(TasksContract.Columns.TASKS_DESCRIPTION , "great");
////        String selection = TasksContract.Columns.TASKS_SORTORDER + " = " + 1;
////        int count = contentResolver.update(TasksContract.CONTENT_URI , values ,selection ,null);
////        Log.d(TAG, "onCreate: " + count + " record(s) upgraded");
//
////        vase update kardan
////        values.put(TasksContract.Columns.TASKS_NAME , "css");
////        int count = contentResolver.update(TasksContract.buildTaskUri(5) , values ,null ,null);//chon update khoroojish int has age deghat koni update male AppProvider has vali contentResolver oon ro seda mizane in khyli mohem ast
////        Log.d(TAG, "onCreate: " + count + " record(s) upgraded");
//
////        baraye ezafe kardan data be database
////        values.put(TasksContract.Columns.TASKS_NAME , "html");//ContentValues object ro mese bundle dar nazar begir ke key value ast inja key has eseme column va value ham hamoon value ast
////        values.put(TasksContract.Columns.TASKS_DESCRIPTION , "for site");
////        values.put(TasksContract.Columns.TASKS_SORTORDER ,1);
////        Uri uri = contentResolver.insert(TasksContract.CONTENT_URI , values);//chon methode insert dar appProvider khoroojish uri ast inja ham uri sakhtim
//
////        Cursor cursor = contentResolver.query(TasksContract.buildTaskUri(2) ,projection , null ,null ,TasksContract.Columns.TASKS_NAME);//buildTaskUri(2) inja mostaghim besh id ro goftim age in ro run koni too logcat mibini ke ba id run shode va TASK_ID(101) run shode
//        Cursor cursor = contentResolver.query(TasksContract.CONTENT_URI ,projection , null ,null ,TasksContract.Columns.TASKS_NAME);//ba control + q bebin query ro
//        //be in deghat kon ke query dar appProvider khoroojish cursor has vase hamin inja Cursor sakhtim masalan age insert mikhastim bayad uri misakhtim
//        if (cursor != null){
//            Log.d(TAG, "onCreate: number of rows" + cursor.getCount());//getCount : Returns the numbers of rows in the cursor.
//            while (cursor.moveToNext()){
//                for (int i = 0 ; i <cursor.getColumnCount() ; i++){
//                    Log.d(TAG, "onCreate: " + cursor.getColumnName(i) + " : " + cursor.getString(i));
//                }
//                Log.d(TAG, "onCreate: =================================================");
//            }
//            cursor.close();
//        }
//
////        AppDatabase appDatabase = AppDatabase.getInstance(this);
////        final SQLiteDatabase db = appDatabase.getReadableDatabase();//SQLiteDatabase has methods to create, delete, execute SQL commands, and perform other common database management tasks.
////        //SQLiteDatabase getReadableDatabase () Create and/or open a database
////        //in nabashe database sakhte nemishe
//
    }



    /**
     *
     *tabe payin vase ine ke about dialog ro ke ba class sakhtam inja implement bayad bekonam yek tabe ooon ro
     *
     */
//    @Override
//    public void cancelAbout() {
//        Log.d(TAG, "cancelAbout: starts");
//    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.menumain_addTask:
                taskEditRequest(null);//chon inja mikhayem ye task besazim besh null mifrestim ta too else bere
                break;
            case R.id.menumain_showDurations:
                break;
            case R.id.menumain_settings:
                break;
            case R.id.menumain_showAbout:
                showAboutDialog();

                /**
                 *AboutDialog aboutDialog = new AboutDialog();
                 *aboutDialog.show(getFragmentManager() , null);
                 * khob in do khate payin chi has ? man oomadam vase in custom dialog mese appdialog class neveshtam va ba class bash kar kardam in khat ha ham vase farakhani class ast
                 */
                break;
            case R.id.menumain_generate:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void showAboutDialog(){//MOHEM : bebin ma in tabe ro neveshtim ye dialog ro neshoone ma bede vali fek konam beshe mese appdialog vasash class nevesht khodet emtehan kon
        View messageView = getLayoutInflater().inflate(R.layout.about ,null ,false);//ma chon darim yek dialog custom misazim bayad in layout ro inflate konim va hamoon tor ke midooni inflate view barmigardooone
        //pas in khat bala vase ma ye view misaze ke emkane namayeshe dialog bashe
        //oon null ke warning mide dar filme 253 bebin che kar karde vali mige faghat vase dialog bayad null bashe vagar na baghieye jaha bayad view bashe
        //alt+enter ro bezan va suppress ro bezan yadet bashe disable inspection ro nazani chon in kollan in nemoone warning ro barmidare vali suppress faghat vase hamoon ja va hamin cod ast(vase az beyn bordane warning)

        AlertDialog.Builder builder = new AlertDialog.Builder(this);//buldin dialog for us

        /**
         * inja hamoon jaie ast ke dar layout about goftim ke nemikhad title va icon bezarim inja besh midim
         * indotta hatman bayad ghable create() bashe
         */
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.alarm_clock);

        builder.setView(messageView); //va chon custom ast oon bala ke inflate kardim hala bayad barash set konim

        /**
         * in tabe ro vase in gozashtam ke vaghti roo button click kard dialog dismiss she bejaie inke ba click be rooye view dismiss she chon payin goftam ke chera in kar khoob nis
         */
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                Log.d(TAG, "onClick: entering messageView.onclick showin = " + mDialog.isShowing());//MOHEM : chera in khato comment kardam ? bebin mDialog.isShowing() hatman yek boolean(true/false) barmigardoone hal age be har dalili mDialog null she in logd baes mishe ke barname crash kone chon mDialog.isShowing() nemitoone null bargardoone ya true ya false
                if(mDialog != null && mDialog.isShowing()){//shaya migi chera in if mohemme bebin dar android nemishe ye chiz ro ghatie goft chon vaghti gooshi rotate mishe momkene etefagh haie beofte pas ma hamishe etminan hasel mikonim
                    mDialog.dismiss();
                }
            }
        });

        mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(true);//Sets whether this dialog is canceled when touched outside the window's bounds. If setting to true, the dialog is set to be cancelable if not already set.
        //age be in tabe bala false bedim mitoonim az inke vaghti be khareje dialog click mishe napadid beshe jologiri konim
        //vali hatta age in method ham nabashe bazam kar mikone chon in defult dialog ast(onDismiss())


        /**
         * MOHEM :
         * inke chera onclick ro comment kardam vase 2 chiz ast : 1)title va icon jozve inflate nistan pas nemishe rooshoon click kard
         * 2)dar in dialog auto link dare va age oonjaie ke chizi neveshte nashode ham clik koni(haveset bashe manzooram az oonjaie ke chizi neveshte nashode jaie ast ke link has va samte rastesh khliye)
         * baz ham mibini ke borwser baz mishe be jaie inke dismiss she chon in nahiye dar ekhtiyare text view ast pas vase dialog haie ke auto link dare behtare ke onclick nazarim
         */
//        messageView.setOnClickListener(new View.OnClickListener() {//in ro vase in gozashtam ke vaghti roosh click shod dissmiss she amma methode onclick vase har view ie ke betoone click she ghabele estefadas
//            //hala age deghat karde bashi goftam view pas chon dar inflate view barmigardoone va chon in view hamoon dialog ast ke inflate shode pas bayad roo in onclick gozasht
//            @Override
//            public void onClick(View view) {
//                Log.d(TAG, "onClick: entering messageView.onclick showin = " + mDialog.isShowing());
//                if(mDialog!= null && mDialog.isShowing()){
//                    mDialog.dismiss();
//                }
//            }
//        });

        TextView textView = (TextView) messageView.findViewById(R.id.about_version);//in messageView ro bayad begim chon in messageView inflate shode about ast ke mikhayem
        textView.setText("V" + BuildConfig.VERSION_NAME);//BuildConfig ham ye chi mese string has ke toosh chizi has ba control boro toosh ta bebini

        final TextView about_url = (TextView) messageView.findViewById(R.id.about_url); //khob dar inja widget textView ke dar layout about_web_url vase api haye zire 21 sakhtim ro peyda mikonim
        if (about_url != null){
            about_url.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);//ACTION_VIEW : This is the most common action performed on data : https://developer.android.com/reference/android/content/Intent.html#ACTION_VIEW
                    String s = ((TextView) view).getText().toString();//albate fek konam beshe jaye (TextView) view) benevisim about_url
                    intent.setData(Uri.parse("http://" + s));//chon uri ast bayad hatman scheme dashte bashim
                    try {
                        startActivity(intent);
                    }catch (ActivityNotFoundException e){//vase inke befahmi az koja fahmidam ke in exeption ro bezaram gozashtam app run she va vaghti crash kard oomadam too log cat didam exeption chi boode
                        Toast.makeText(MainActivity.this , "no browser application found, can not visit this site" ,Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        mDialog.show();
        /**
         * in dialog dockme onPositive va onNegative nadare
         */
    }

    //bebin aslan nemidooonam chera inja dare implement mikone khob hatman film 227 ro bebin
    //vali fek mikonam vase in inkaro karde ke betoone be in methode taskEditRequest dastresi dashte bashe chon age in method ro static konim error bemoon dade mishe
    //pas oomad inja implement kard ke betoone in karo bokone
    //baz ham nemidoonam chera in karo karde mishe dakhele hamoon onClick nevesh
//    Intent detailIntent = new Intent(view.getContext , AddEditActivity.class);
//    detailIntent.putExtra(Tasks.class.getSimpleName() , tasks);
//    view.getContextstartActivity(detailIntent);
    //vali nemidoonam chera in kar karde
    //age too diagramie(hamoon axs ke neshoon mide mikhad che kar kone ) ke dare in bakhsh bebini mibini ke onclick besh call back dade be main activity


    @Override
    public void onEditClick(Tasks tasks) {//amma chera in tabe haro inja neveshtim chera too ye class dg naneveshtim chon in class baes mishe ke edit ya delete etefagh biyofte chon too in class hastan
        //yani az tarighe in class mishe oon ha ro seda zad(too layoutesh image ha hastan)
        Log.d(TAG, "onEditClick: starts");
        taskEditRequest(tasks);
    }

    @Override
    public void onDeleteClick(Tasks tasks) {
        Log.d(TAG, "onDeleteClick: starts");

        AppDialog dialog = new AppDialog();
        Bundle args = new Bundle();
        args.putInt(AppDialog.DIALOG_ID, DIALOG_ID_DELETE);//age in khat nabashe error migirim hamchenin khate payin chon dar appDialog besh goftim ke in dotta bundle zaroori has
        args.putString(AppDialog.DIALOG_MESSAGE, getString(R.string.deldialog_message, tasks.getId(), tasks.getName()));//amma tasks.getId() , tasks.getName() chi hastan : chon dar R.string.deldialog_message age beri bebini
        //placeholder gozashim(%1$d , %2$s) in dotta ro gozashtim vase in placeholder ha
        args.putInt(AppDialog.DIALOG_POSITIVE_RID, R.string.deldialog_positive_caption);//inaj dar voroodi dovoom ke R.id has avval neveshte boodam "delete" bad ba alt + enter kardamesh too String bad khode android studio
        //vase man ghables getString gozasht yani intor : getString(R.string.deldialog_positive_caption) vali chon dar dialog in bayad int bashe va ma ham ghablesh neveshtim args.putINt in bayad int she
        //yani bayad getString ro pak kard va khode R.string.deldialog_positive_caption ye int barmigardoone

        /**
         * amma chera deldialog_negative_caption ro nazashtim chon mikhayem hamoon defualt ke dar appDialog has (cancel ) hamoon bashe vase hamin barash id dar nazar nagereftim
         */


        /**
         * MOHEM : khob alan ma ye moshkel inja darim oonam inke content resolve ke vase pak kardan task bood inja bood va bad ma pakesh kardim va bordemish be jaye asli yani onPositiveDialogResult
         * vali dar inja yani onPositiveDialogResult chizi az task nemidoone ke bakhd id oon ro begire vali dar inja midoonest hala bayad dar inja ye too bundle in id ro zakhire konim (id task)
         * va bedimesh be onPositiveDialogResult ke darr zir in karo kardam
         */
        args.putLong("TaskId", tasks.getId());

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), null);//in null vase tag has ke age khasti fragment ro ba tag peyda koni


    }

    //for appDialog.dialogEvent
    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {//dar in tabe yani user mikhad delete kone
        Log.d(TAG, "onPositiveDialogResult: called");
        switch (dialogId){//switch gozashtim chon dar inaj ma dotta dialog darim ke fargh daran ba ham va mikhayem ke onPositive va onNegative an ha ro ba ham betoonim handle konim vase hamin ma be dialog ha id dadim
            case DIALOG_ID_DELETE:
                Long taskId = args.getLong("TaskId");//khob ma chon content resolver chizi az task nemidoonest chon id oon ro mikhast vase sakhte uri az bundle estefade kardim va dadimesh be args va inja gereftimesh
                //getLong :Returns the value associated with the given key, or 0L if no mapping of the desired type exists for the given key.
                //MOHEM : sqlite hichvaght ye data ba _id 0 nemisaze amma dar bala didim ke age getLong chizi nadashte bashe (hala tahte har sharayeti) oon ro 0 mide ke ma bayad az in kar jologirii konim

                //amma khate payin chist : daghigh nemidoonam BuildConfig.DEBUG chiye amma ye joor boolean ast mese inke va migim ke age in khat true bood ke exception bendaz
                //albate ke mishe ba rah haye dg ham in karo kard
                if (BuildConfig.DEBUG && taskId == 0) throw new AssertionError("task id is 0");
                getContentResolver().delete(TasksContract.buildTaskUri(taskId), null, null);
                break;

            case DIALOG_ID_CANCEL_EDIT:
                //no action required
                //age kari nakoni khdoesh tabe disMiss ejra mishe defualt (hata age ham biroon dialog box click koni disMiss() ejra mishe)
                break;
        }

    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        Log.d(TAG, "onNegativeDialogResult: called");
        switch (dialogId){
            case DIALOG_ID_DELETE:
                //no action required
                break;
            case DIALOG_ID_CANCEL_EDIT:
                finish();//chon age nakhad chizi ro save kone bayad barname baste she ke ba in finish in karo mikonim
                break;
        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {//in tabe ro gozashtim vaghat vase log kardan mitoooni roosh bishtar tahghigh koni
        //faghat yadet bashe age roo onNegative zadi tabe onNegativeDialogResult farakhani mishe badesh onDismiss hatman farakhani mishe
        //va age khareje dialog box click kardi onCancel ke dar app dialog ast seda zade mishe ke ma azesh vase in tabe callback gereftim va dobare badesh onDissmiss farakhani mishe
        Log.d(TAG, "onDialogCancelled: called");

    }
    //================================

    @Override
    public void onSaveClicked() {
        Log.d(TAG, "onSaveClicked: starts");
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.task_details_container);
        if (fragment != null) {
//            getSupportFragmentManager().beginTransaction().remove(fragment).commit(); moadele cod payin ast
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
            if (mTwoPane) {
                findViewById(R.id.task_details_container).setVisibility(View.GONE);
            }
        }
    }

    private void taskEditRequest(Tasks tasks) {
        Log.d(TAG, "taskEditRequest: starts");
        if (mTwoPane) {
            Log.d(TAG, "taskEditRequest: in two-pane mode(tablet)");
            findViewById(R.id.task_details_container).setVisibility(View.VISIBLE);
            AddEditActivityFragment fragment = new AddEditActivityFragment();//https://developer.android.com/guide/components/fragments.html#Managing in link vase kar ba fragment has
            //dar in link mige age bekhaye fragment add ya delete koni (transaction) bayad avval fragmentManager ro farakhani koni

            /**
             * khob in cod alan kar mikone vali na be dorosti chon alan vaghti ke mikhaye ye chizi ro edit koni neshoonesh mide vali chon dar in if fragment ma chizi az tasks nemidoone fild haye edit khli ast
             * pas ma bayad ye kari mese else payin ke ba intenPutExtra in karo kardim va be fragmen add edit az task ettela dadim inja ham in karo bekonim vali chon intent nadaarim bayad Bundle besazim
             * intenputextra ham bundle ast hal ma bayad yek bundle besazim
             */

            Bundle arguments_for_addeditActivityFragment = new Bundle();
            arguments_for_addeditActivityFragment.putSerializable(Tasks.class.getSimpleName(), tasks);//ma chon darim az bundle estefade mikonim bayad key value bashe amma chon mikhayem ye object besh befrestim bayad in object serializeable bashe hatman
            //pas har vaght ke khsti ye object az class befresti hatman oon bayad serializeable bashe bedin mani ast ke miyad object ro be byte tabdil mikone bad oon ro bazgardooni mikone
            //chize dg ham inke ma vase key az esme class estefaade kardim
            fragment.setArguments(arguments_for_addeditActivityFragment);//void setArguments(android.os.Bundle args) : This method ---> cannot be called if the fragment is added to a FragmentManager and if isStateSaved() would return true.
            //chon mige ke in methos nemitoone bade inke be fragment manager ezafe shod seda zade she ma oon ro ghable manager neveshtim

            FragmentManager fragmentManager = getSupportFragmentManager();//chera nagoftim getFragmentManager ? goftim getSupportFragmentManager(support) chom mikhayem api haye ghabli ham support kone
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.task_details_container, fragment);//FragmentTransaction replace (int containerViewId,Fragment fragment)
            //khob chera goftim replace nagoftim add : add ham dorost ast yani kar mikone vali ye bug riz dare age ejra koni barname ro va yeki ro eddit koni vase dovvomi ke mikhaye ediit kone roo avvali miyofte
            //yani nemiyad avvali ro pak kone va in jadide ro neshoon bede roo ham neshoon mide vase hamin az replace estefade kardam
            //Replace an existing fragment that was added to a container. This is essentially the same as calling remove(Fragment) for all currently added fragments that were added with the same containerViewId and then add(int, Fragment, String) with the same arguments given here.
            //pas harvaght khastim ye kare mese in bokonim ke hey bayad fragment ha ro add konim bayad az replace estefade konim chon ghabli ro pak mikonim va yek chiz e jadid ezafe mikone
            //https://developer.android.com/reference/android/app/FragmentTransaction.html#add(int, android.app.Fragment, java.lang.String)  in ham linkesh
            //hatmane hatman bayad badesh commit zad
            fragmentTransaction.commitNow();//https://developer.android.com/reference/android/app/FragmentTransaction.html#commit() vase commit khoob ast

        } else {
            Log.d(TAG, "taskEditRequest: in single mode (phone)");
            Intent detailIntent = new Intent(this, AddEditActivity.class);
            /**
             * dar inja besh migim ke age too mtwo-pane naboodi yani too portrait hasti biyad ba intent boro be addeditActivity(na be fragmentesh) ba ba intent barash data ferestadim
             * bad in mire be addeditactivity va oonja data ro migire va az oonja mire be fragmentesh
             */
            if (tasks != null) { //edditing a task
                detailIntent.putExtra(Tasks.class.getSimpleName(), tasks);//vase hamin class Tasks ro serialable karidm
                //inja name class task key has
//                Log.d(TAG, "taskEditRequest: qqqqqqqqqqqqqqqqqqqqqqqqq" + Tasks.class.getSimpleName());//getSimpleName : the simple name of the underlying class
                startActivity(detailIntent);
            } else { //adding a new task
                startActivity(detailIntent);
            }
        }
    }

    @Override
    //in tabe miyad vaghti ke dockme back (ke payin gooshi ast) zade mishe farakhani mishe va chon dar oon tab ie nis vaghti ejra mishe mire mibine ka superclass main chi has va chon chizi nis app ro mibande
    public void onBackPressed() {//in method ham call back ast az tarafe android chon ma dar inja oon ro neveshtim vaghti ke seda zade mishe dar inja midoone ke super class oon chi has ke bargarde be oon
        Log.d(TAG, "onBackPressed: called");
        /**
         * amma ma mikhayem che kar konim dar in tabe?
         * ma mikhayem vaghti gooshi dar landscape has va darim ye task ro edit mikonim (pas yani fragment addeditactivityFragment run shode) age user dockme back ro zad barash
         * ye dialog neshoon bedim ke aya mikhad taghirato save kone ya na
         * amma chera faghat dar landscape ? chon dar portrait mode va addeditfragment ro too ye activity dg baz mikonim ke age user back ro zad barmigarde be main pas app baste nemishe vali dar
         * land age back ro zad app baste mishe chon ke addeditfragment ro dare dar main neshoon mide
         * hal chera vase inke befahmim ke aya fragmentaddedit vojood dare nagoftim if(mtwopane) ? chon age faghat hamin ro begim be ma mige ke are gooshi dar landscape has hamino o bas
         * vali chon ma fargment ro kardim too ya frame layout be esme task_details_container age begim if(mtwopane) faghat mige ke are in gooshi dar land ast va ma nemitoonim befahmim
         * ke aya alan fragment jaygozine frame layout shode ya na vali bayad az koja fahmid ? age degaht koni dar methode taskEditRequest besh goftim ke age too twopane mod bood
         * biyad va fragment ro replace frame layout kon pas inja mifahmim ke fragment jabe ja shode hala vaghti dar in tabe miakhyem bedoonim ke aya fragment jaygozin shode ya na kafi ast
         * ke findfragmetnbyid konim
         */
        FragmentManager fragmentManager = getSupportFragmentManager();

        /**
         * khate payin findFragmentById yek fragment barmigardoone va hatman ham bayad daroone yek fragment zakhire shavad hamoon tor ke masalan getLong ye long barmigardoone
         * ma tooye ye moteghayere long zakhirash mikonim pas inja fagaht mitoone AddEditActivityFragment bashe ya MainactivityFragment ke bayad AddEditActivityFragment bashe
         * va tabe canClose ha ke bayad dar AddEditActivityFragment bashe chon ma yek instance az AddEditActivityFragment sakhtim
         * age benevisi MainActivityFragment fragment = (MainActivityFragment) fragmentManager.findFragmentById(R.id.task_details_container); khata migire va hatman khatash ro bebin mige ke:
         * java.lang.ClassCastException: com.example.ali.tasktimer.AddEditActivityFragment cannot be cast to com.example.ali.tasktimer.MainActivityFragment
         * chera chon R.id.task_details_container
         * az addedit fragment has ke too mikhaye be main activityfragment tabdilesh koni pas har chi sare jaye khod
         */
        AddEditActivityFragment fragment = (AddEditActivityFragment) fragmentManager.findFragmentById(R.id.task_details_container);//findFragmentById() : Returns:The fragment if found or null otherwise.
        //in hamoon jaie ast ke mifahmim ke aya fragment jaygozine fram layout shode ya na
        //vase peyda kardane fragment hamin karo bayad kard
        //chon findFragmentById niyaz be fragmentManager dare va chon in ye fragment barmigardoone bayad dar yek fragment rikhte she
        if ((fragment == null) ||fragment.canClose()){//in hamoon if ie bood ke dar bala goftam vase inke befahmim ke aya fragment has ya na ya inke faghat dare liste task ha ro neshoon mide
            /**MOHEM : in khyli mohem ast ke sharayete if ro che tor minevisim inja chon or ast avval miyad sharte avvla ro check mikone bad dovvomi ro pas behtare in ke null hast ro avval bezarim ke age ye
             * vaght null bood sari tar anjam she ta inke avval bere too ye tabe bad biyad null ro check kone pas havaset bashe ke sharayet be tartib anjam mishe
             */
            //in canClose yek tabe ast ke minevesim vali in hatman bayad rooye yek instance az fragment anjam shavad pas yeki az dalil haye dg ke fragment ro cast kardim hamin bood
            super.onBackPressed();//agar in tabe ro dar in method pak koni kar nemikone in method dar vaghe in khat baes mishe ke activity be parentesh bargarde va age ham ke khodesh parent bashe baste she app
        } else {
            //show dialog to get confirim to quite the edit
            //in tike codde payin moshabeh onDeleteClick ast ke toosh dialog sakhtim
            AppDialog dialog = new AppDialog();
            Bundle args = new Bundle();
            args.putInt(AppDialog.DIALOG_ID , DIALOG_ID_CANCEL_EDIT);
            args.putString(AppDialog.DIALOG_MESSAGE , getString(R.string.cancelEditDialog_message));
            args.putInt(AppDialog.DIALOG_POSITIVE_RID , R.string.cancelEditDialog_positive_caption);
            args.putInt(AppDialog.DIALOG_NEGATIVE_RID , R.string.cancelEditDialog_negative_caption);

            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), null);//getSupportFragmentManager ro vase in gozashtim ke dar appDialog az library compat estefade karim

        }
    }

    @Override
    //vaghti ba class neveshtam in dialog ro dg niyaz be in tabe nis chon oon moghe khodesh handle mikone
    protected void onStop() {//in tabe ro vase in neveshtim -->  MainActivity has leaked window  <-- zamani ke app ro run mikonim va about ro neshoon midim hala age dar hamin halat rotate konim
    //too logcat ye error mide ke mige memory leaked dare va in zamani ast ke garbuge calectoure java nemitoone kari kone pas ma bayad zamani ke rotate mishe(tebghe life sycle onstop farakhani mishe) dialog ro dissmiss konim
        Log.d(TAG, "onStop: starts");
        super.onStop();
        if (mDialog != null && mDialog.isShowing()){//isShowing : Returns : boolean Whether the dialog is currently showing.
            mDialog.dismiss();
        }
    }
}
