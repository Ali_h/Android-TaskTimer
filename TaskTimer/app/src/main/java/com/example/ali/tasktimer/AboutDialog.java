package com.example.ali.tasktimer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Created by ali on 8/5/17.
 * this class is another way to create the custom dialog
 *
 * https://developer.android.com/guide/topics/ui/dialogs.html#CustomLayout in link vase sakhte in class
 *
 * faghat ye farghi ke dare ke ba class neveshtam ine ke vaghti dialog neshoon dade shode vaghti rotate mikonam az beyn nemire vali dar oon mishe
 */


public class AboutDialog extends DialogFragment{
    private static final String TAG = "AboutDialog";
    public static final String DIALOG_ID = "dialog_id";

    interface CancelAbout{
        void cancelAbout();
    }
    CancelAbout mCancelAbout;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CancelAbout)) {
            throw new ClassCastException(context.toString() + " must implement DialogEvent interface");
        }
        mCancelAbout = (CancelAbout) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCancelAbout = null;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {//in tabe ro serfan neveshtam ke callback ro yad begiram
        Log.d(TAG, "onDismiss: starts");
//        super.onDismiss(dialog);
        if (mCancelAbout != null){
            mCancelAbout.cancelAbout();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog: called");
        //daghighan hamoon kari ke dar showAboutDialog() too main kardim ro hamin ja mikonim in daghighan hamoontore vali ba class neveshtim

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View aboutView = inflater.inflate(R.layout.about, null);//ghaziye in null ro peygiri kon

        builder.setView(aboutView).setTitle(R.string.app_name)
                                  .setIcon(R.mipmap.alarm_clock);

        TextView textView = (TextView) aboutView.findViewById(R.id.about_version);
        textView.setText("v" + BuildConfig.VERSION_NAME);

        return builder.create();
    }
}
