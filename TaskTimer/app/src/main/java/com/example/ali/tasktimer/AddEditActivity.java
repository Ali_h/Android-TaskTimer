package com.example.ali.tasktimer;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

public class AddEditActivity extends AppCompatActivity implements AddEditActivityFragment.OnSaveClicked , AppDialog.DialogEvent{//inja az AddEditActivityFragment.OnSaveClicked implement kardim chon too portrait fragment too in neshoon dade mishe
    private static final String TAG = "AddEditActivity";
    public static final int DIALOG_ID_CANCEL_EDIT = 1;//in vase id dialog ast ke dar in activity estefade mishe albate inam begam ke DIALOG_ID_CANCEL_EDIT be hamin esm dar mainActivity ham dade shode bood vali mohem nis chon in yek activity digar ast

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);//setContentView ham inflate mikone by defualt
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//vase ine ke besh goftim bargard be parentet hamoon dockme back has

        AddEditActivityFragment fragment = new AddEditActivityFragment();//https://developer.android.com/guide/components/fragments.html#Managing in link vase kar ba fragment has

        Bundle arguments = getIntent().getExtras();

        fragment.setArguments(arguments);//void setArguments(android.os.Bundle args) : This method ---> cannot be called if the fragment is added to a FragmentManager and if isStateSaved() would return true.

        FragmentManager fragmentManager = getSupportFragmentManager();//chera nagoftim getFragmentManager ? goftim getSupportFragmentManager(support) chom mikhayem api haye ghabli ham support kone
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment , fragment);//FragmentTransaction replace (int containerViewId,Fragment fragment)
        fragmentTransaction.commit();

        /**
         * MOHEM :
         * age roo portrait boodim ke ba intent be inja vasl mishe va bemoon data mide va ma oomadim inja data ro (bundle)gereftim va dadim be fragment
         * vali age roo landscape bashe khodesh too main Activty mostaghim vasl shode be addeditactivityFragment va besh ham data dade
         * amma ye chize mohem inke age ma land scape run konim didim ke too land/content_add_edit yek fram layout gozashtim ke ye jooraie placeholder ast va nemayesh dade nemishe ta zamani ke ye chi az
         * too cod besh ezafe konim
         * hala vase portrait ham hamin karo mikonim yani vase content_main miayem fragment ro too ye fram layout mizarim ke inkaro ba xml mikonim va ba avaz kardane tag fragment be framelayout
         * hala chera in karo mikonim age in karo nakonim in ja miyad va setContentView(R.layout.activity_add_edit); mikone in ham mire va fragment ro neshoon mide ba payin ham baz besh goftim ke
         * too ie fragment in fragment ro neshoon bede yani do bar neshoon mide pas khoobe ke placeholder beshe(too farz kon oon content_add_edit (landesh ro na) ke too portrait ejra mishe ye jooraie mese hamin
         * land too khode main_activity neshon dade mishe ke dorostesh ham hamine bebin beza vazeh begam intori ast ke age ja dashte bashe too khode activity neshoon dade mishe age nadashe bashe too khode activity
         * has amma dar ye safhe dg neshoon dade mishe pas ma bayad in ro place holder konim ke ta zamani ke besh nagoftim fragment ro ezafe nakon in karo nakone va dobbar neshoon dade nashe
         */
    }

    @Override
    public void onSaveClicked() {
        Log.d(TAG, "onSaveClicked: starts");
        finish();//chera inja remove nakardim khob chon ma darim addedit fragment ro mibinim roo gooshi age ke remove konim ke safhe sefid be ma neshoon mide pas bayad finish konim ke bere be activity ghabl
    }

    //for app dialog
    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {//vaghti roo in dockme zad usesr bayad be kare edit edame dahad pas niyazi nis inja kari kard chon khodesh onDissmiss mishe
        Log.d(TAG, "onPositiveDialogResult: called");

    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {//in dockme ke ham zade she bayad in activity baste she
        Log.d(TAG, "onNegativeDialogResult: called");
        finish();

    }

    @Override
    public void onDialogCancelled(int dialogId) {
        Log.d(TAG, "onDialogCancelled: called");

    }
    //==============================================

    @Override
    public void onBackPressed() {//in tabe dorost mese onBackPressed dar main Activity ast baraye tozihate bishtar be oonja moraje kon
        Log.d(TAG, "onBackPressed: called");

        FragmentManager fragmentManager = getSupportFragmentManager();//bale dorost ast dar inja ham bayad fragment ro peyda kone
        AddEditActivityFragment fragment = (AddEditActivityFragment) fragmentManager.findFragmentById(R.id.fragment);//vali yekhorde roo inja roosh fek kon tabe haro tajziye tahlil kon
        if (fragment.canClose()){//digar nemikhad null check she chon hatman vaghti addedit activity run mishe fragmentesh ham run mishe
            super.onBackPressed();
        } else {
           showConfirmationDialog();
        }
    }

    private void showConfirmationDialog(){
        AppDialog dialog = new AppDialog();
        Bundle args = new Bundle();
        args.putInt(AppDialog.DIALOG_ID , DIALOG_ID_CANCEL_EDIT);
        args.putString(AppDialog.DIALOG_MESSAGE , getString(R.string.cancelEditDialog_message));
        args.putInt(AppDialog.DIALOG_POSITIVE_RID , R.string.cancelEditDialog_positive_caption);
        args.putInt(AppDialog.DIALOG_NEGATIVE_RID , R.string.cancelEditDialog_negative_caption);

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager() , null);//getSupportFragmentManager ro vase in gozashtim ke dar appDialog az library compat estefade karim
    }

    /**
     *
     *MOHEM : barkhord ba home button dorost mese onbackpressed ast ba in farghe ke oon dar onOptionsItemSelected anjam mishe
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {//in tabe ro vase in gozashtam ke vaghti ham roo dockme home ke balaye safhe too toolbar ast ke barmigarde be activity ghabl hatta age oonamzad
        //mese in up button bash barkhord she vali chizi ke has id in home ro ma nemidim balke khode android mide va in tor ast android.R.id.home
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d(TAG, "onOptionsItemSelected: home button pressed");
                AddEditActivityFragment fragment = (AddEditActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
//                Log.d(TAG, "onOptionsItemSelected: ttttttttttttttttttt" + super.onOptionsItemSelected(item)); in super.onOptionsItemSelected(item) false barmigardoone be in manzoor ke item menu kare mamool khod ra anjam bede age true bargardoonim yani khodemoon handle kardim va mohem ham has ke age bekhayem kari konim true bargardoonim chon age nakonim khodesh kare mamool khodesh ro mikone
                if (fragment.canClose()) {
                    return super.onOptionsItemSelected(item);//dar vaghe in khat mige be item menu ke hamegi kare mamool khod ra anjam bedahid (ke false ham has too document gofte)
                } else {
                    showConfirmationDialog();
                    return true;//age ma khodemoon item menu ro handle konim true barmigardoonim ke kare mamool khod ra anjam nadahad
                }
            default://chon in tabe yek boolean return mikone va momken ham has ke aslan too in case ha nabashe chize mored nazar khob pas taklifesh chiye ke alan hichi return nemikone pas bayad too defualt in kar kard
                return super.onOptionsItemSelected(item);
        }
    }
}
